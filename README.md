# dpp - the docker preprocessor
[Docker](https://docker.com) creates versioned, distributable self-contained environments for software based on *contexts*, folders containing data needed by the container environment, and a *[Dockerfile](https://docs.docker.com/engine/reference/builder/)*, a build script written in a built-in language. However, the Dockerfile language is limited by design to the barest essentials, and as such forces its users to maintain copious boilerplate and copypasta for nontrivial applications. 

This project is a preprocessor written in Python that reads a superset of the Dockerfile syntax (called `Dockerfile.in`) and fills in the boilerplate, generating a complete Dockerfile and context which can be fed into the docker engine.


## Limitations
* I banged out dpp in a weekend, and it shows. Expect breaking changes as I improve some things, refactor others, write a better parser, backfill docstrings, etc.

* Currently dpp is very debian-specific. I have plans to eventually support alpine, arch and centos based containers as well, whenever time permits.

* Output locations are hardcoded (and rather unhelpful). Command line flags and configurable output are coming very soon.


## Usage

`python -m dpp path/to/context`

Inside `path/to/context` dpp will look for a file inside your context folder called `Dockerfile.in` or just `Dockerfile` and use it to generate a native dockerfile and an output context, which it will save to `.dpp/contexts`.


# Dockerfile.in Syntax

## Changed Commands
* `ADD` and `COPY` now work with absolute and relative paths outside the docker context. dpp will copy any files it needs into the output context.
* `FROM` is only allowed once. After the first `FROM`, subsequent `FROM`s are ignored.


## New Commands
dpp adds several extra commands to the dockerfile syntax, most of which are convenience methods for commonly-used idioms.

### APT
```
APT package_name [package_name...]
```
_Install apt packages according to Docker best practices._

Docker's documentation explains in detail the correct way to invoke `apt-get install` [in a dedicated section of the best-practices guide](https://docs.docker.com/engine/userguide/eng-image/dockerfile_best-practices/#apt-get). This command is shorthand for that.

**Dockerfile.in**
```
APT curl
```

**Output Dockerfile**
```
RUN apt-get -qq update --fix-missing \
 && apt-get -qqy -o=Dpkg::Use-Pty=0 install curl \
 && rm -rf /var/lib/apt/lists/*
```


### IMPORT
```
IMPORT /path/to/context
```
_Include a subset of commands and files from another Docker context._

Import is similar to a block include (described below) but does some additional postprocessing to prevent commands in the included dockerfile from messing with your own. This is an effort to provide a multiple-inheritance-like experience to Docker, allowing you to include other Docker environments after you've already used your `FROM`.

Import suppresses `CMD`, `LABEL`, `EXPOSE`, `ENTRYPOINT`, `FROM` and `MAINTAINER` in the imported Dockerfile, and reverses any changes to `WORKDIR` or `SHELL` after it completes. It also immediately executes anything flagged `ONBUILD`.

**/other/Dockerfile**
```
FROM ubuntu:xenial
ADD setup.sh /foo
WORKDIR /foo
RUN ./setup.sh
EXPOSE 80
```

**Dockerfile.in**
```
FROM debian:sid
IMPORT /other
RUN mkdir -p opt/local/bin
```

**Output Dockerfile**
```
FROM debian:sid
ADD other/setup.sh /foo
WORKDIR /foo
RUN ./setup.sh
WORKDIR /
RUN mkdir -p opt/local/bin
```


### PIP2 and PIP3
```
PIP2 package_name [package_name...]
PIP3 package_name [package_name...]
```
_Install pip packages._

These commands are shorthand-installers for [pip](https://pip.pypa.io/en/stable/) packages; `PIP2` for python2.x and `PIP3` for python3.x. They check whether the appropriate version of pip has already been installed, install it if not, and then install the listed packages.

These commands use setuptools rather than apt to install pip, to avoid getting Debian's [bad pip patch](http://sources.debian.net/patches/python-pip/8.1.2-2/hands-off-system-packages.patch/).

**Dockerfile.in**
```
PIP2 boto3
```

**Output Dockerfile**
```
RUN if ! which pip2 >/dev/null; then 
    apt-get -qq update --fix-missing && \
    apt-get -qy -o=Dpkg::Use-Pty=0 install python-setuptools && \
    rm -rf /var/lib/apt/lists/* && \
    easy_install pip && \
    pip2 install --upgrade pip; fi && \
 pip2 install boto3
```

### PREPARE
```
PREPARE command
```
_Execute a shell command on the local system._

This will run `command` immediately on your local system. The command will be executed in the output context folder (that is, the docker context that dpp is generating), with the `$SOURCE` environment variable set to the source context that dpp is reading from.

**Dockerfile.in**
```
ADD /path/to/my/code /codebase
PREPARE git clean -xfd code
```

**Output Dockerfile**
```
ADD code /codebase
```


### SCRIPT
```
SCRIPT file
```
_Add and run a file from the context._

This is simple shorthand for `COPY` and `RUN`.

**Dockerfile.in**
```
SCRIPT install.sh --with-everything
```

**Output Dockerfile**
```
COPY install.sh /
RUN cd / \
 && chmod +x ./install.sh \
 && ./install.sh --with-everything
```


## String Interpolation Syntax
### Block Includes
```
#<path/to/include>
```

If this is the only thing on the line, the line is read as a block-level include. The parser will read the file given by the `path/to/include` and insert its commands in the output.

If `path/to/include` is a directory, dpp will look for a `Dockerfile.in` or `Dockerfile` inside the directory.


### Inline Includes
```
RUN #<path/to/script>
```

The syntax is the same as a block include, but when it appears later in the line, it is treated as an inline include. The given path will be inserted inline with its line breaks escaped.

**Dockerfile.in**
```
RUN #<things.txt>
```

**things.txt**
```
echo foo
echo bar
```

**Output Dockerfile**
```
RUN echo foo \
    echo bar
```

Because of how docker handles escaped newlines, this will output `foo echo bar` when it is run.

### Environment Variables
```
#{NAME}
#{NAME:-defaultvalue}
```

This will be replaced with the value of the environment variable given by `NAME` at the time of the dpp run. The second form will insert `defaultvalue` if the requested environment variable isn't defined.

**Dockerfile.in**
```
ENV EDITOR #{EDITOR:-nano}
ENV JAVA_HOME #{JAVA_HOME:-/usr/share/java}
```

**Output Dockerfile**
```
ENV EDITOR vim
ENV JAVA_HOME /usr/share/java
```

### Shell Commands
```
#(command)
```

This will execute the `command` in parenthesis in a shell and insert that command's STDOUT output.

**Dockerfile.in**
```
MAINTAINER #(git config user.name) <#(git config user.email)>
```

**Output Dockerfile**
```
MAINTAINER Allison Bob <allison.bob@example.com>
```

