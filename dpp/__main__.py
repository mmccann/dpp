#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os.path import abspath

import dpp.commands
from dpp.compiler import Compiler
from dpp.preprocessor import Preprocessor
from dpp.core import FileReader


def main(argv):
    compiler = Compiler(Preprocessor(FileReader()))
    for input_path in [abspath(p) for p in argv[1:]]:
        max(compiler.compile(input_path))



if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv) or 0)
