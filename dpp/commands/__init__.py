#!/usr/bin/env python
# -*- coding: utf-8 -*-

from dpp.commands.add import Add, Copy
from dpp.commands.apt import Apt
from dpp.commands.from_ import From
from dpp.commands.import_ import Import
from dpp.commands.pip import Pip2, Pip3
from dpp.commands.prepare import Prepare
from dpp.commands.script import Script
from dpp.commands.workdir import Workdir
