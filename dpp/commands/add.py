#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os.path import relpath

from dpp.core import Command


class LoaderCommand(Command):
    def __init__(self, *a, **kw):
        super(LoaderCommand, self).__init__(*a, **kw)
        self.args = self.compiler.split_files(self.args)
        self.dest = self.args.pop() if len(self.args) > 1 else None
        self.copies = [
            arg if "://" in arg else self.compiler.add_context(arg, self.dest)
            for arg in self.args
        ]
        if not self.dest:
            self.dest = self.scope['workdir']

@Command.register
class Add(LoaderCommand):
    def apply(self):
        yield ('ADD ', ' '.join(self.copies), self.dest)


@Command.register
class Copy(LoaderCommand):
    def apply(self):
        yield ('COPY ', ' '.join(self.copies), self.dest)
