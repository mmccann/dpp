#!/usr/bin/env python
# -*- coding: utf-8 -*-

from dpp.core import Command


@Command.register
class Apt(Command):
    def apply(self):
        yield ('RUN', ' && '.join([
            'apt-get -qq update --fix-missing',
            'apt-get -qqy -o=Dpkg::Use-Pty=0 install %s' % self.args,
            'rm -rf /var/lib/apt/lists/*'
        ]))
