#!/usr/bin/env python
# -*- coding: utf-8 -*-

from dpp.core import Command


@Command.register
class From(Command):
    def apply(self):
        if From not in self.scope:
            self.scope[From] = self
            yield ('FROM %s' % self.args,)
