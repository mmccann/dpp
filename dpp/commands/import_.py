#!/usr/bin/env python
# -*- coding: utf-8 -*-

from collections import OrderedDict
from itertools import chain
import re

from dpp.core import Command

@Command.register
class Import(Command):
    def apply(self):
        originals = {k: self.scope[k.lower()] for k in {'WORKDIR', 'SHELL'}}
        restore = OrderedDict()
        suppress = {'CMD', 'LABEL', 'EXPOSE', 'ENTRYPOINT', 'FROM',
                    'MAINTAINER'}
        onbuilds = []
        for line in chain(self.compiler._compile(self.args), onbuilds):
            cmd, args = self.compiler.split_args(line)
            if cmd in originals:
                restore[cmd] = originals[cmd]
            if cmd == 'ONBUILD':
                onbuilds.append(re.sub(r'(?i)^(\s*)ONBUILD\s*', '\\1', line))
            elif cmd not in suppress:
                yield (line,)

        for key, value in reversed(restore.items()):
            self.scope[key.lower()] = value
            yield (key, value)
