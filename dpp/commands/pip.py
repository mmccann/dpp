#!/usr/bin/env python
# -*- coding: utf-8 -*-

from dpp.core import Command


class PipCommand(Command):
    def apply(self):
        suffix = '' if self.python == '2' else '3'
        cmd = 'pip%s' % self.python
        yield ('RUN', ' && '.join([
            'if ! which %s >/dev/null; then %s; fi' % (
                cmd,
                ' && '.join([
                    'apt-get -qq update --fix-missing',
                    'apt-get -qy -o=Dpkg::Use-Pty=0 install ' +
                        'python%s-setuptools' % suffix,
                    'rm -rf /var/lib/apt/lists/*',
                    'easy_install%s pip' % suffix,
                    '%s install --upgrade pip' % cmd
                ])
            ),
            " ".join([cmd, 'install', self.args])
        ]))


@Command.register
class Pip2(PipCommand):
    python = '2'


@Command.register
class Pip3(PipCommand):
    python = '3'
