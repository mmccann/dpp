#!/usr/bin/env python
# -*- coding: utf-8 -*-

from itertools import chain
from os.path import basename
import re

from dpp.core import Command


@Command.register
class Script(Command):
    def __init__(self, *a, **kw):
        super(Script, self).__init__(*a, **kw)
        self.args = self.compiler.split_files(self.args)
        self.copies = []
        for optional, arg in enumerate(self.args):
            try:
                self.copies.append(self.compiler.add_context(arg))
            except (OSError, IOError):
                if not optional:
                    raise

    def apply(self):
        yield ('COPY', " ".join(self.copies + [self.scope['workdir']]))
        yield ('RUN', ' && '.join([
            'cd %s' % self.scope['workdir'],
            'chmod +x ./%s' % self.args[0],
            'sync',
            './%s' % self.args[0] + ' ' + ' '.join(self.args[1:])
        ]))
