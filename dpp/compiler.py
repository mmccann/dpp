#!/usr/bin/env python
# -*- coding: utf-8 -*-

from itertools import chain
from json import dumps
from os import chdir, getcwd, link, makedirs
from os.path import (abspath, basename, dirname, exists, isabs, isdir, join,
                     relpath, samefile)
import re
from shutil import copy2, copytree, rmtree

from dpp.core import Command, FileReader

PRIVATE = re.compile(r'^__(.*)__$')


try:
    basestring
except NameError:
    basestring = (str, bytes)


def trylink(src, dest):
    try:
        link(src, dest)
    except:
        print("Cannot hardlink %r => %r; copying instead." % (src, dest))
        copy2(src, dest)


class SourceFile(Command):
    name = 'SOURCE_FILE'

    def apply(self):
        self.compiler.context_stack.append(self)
        self.compiler.file_stack.append([abspath(self.args.strip()), 1])

    def remove(self):
        self.compiler.file_stack.pop()


class End(Command):
    def apply(self):
        return self.compiler.context_stack.pop().remove()


class With(Command):
    def apply(self):
        command, args = self.compiler.split_args(self.args)
        instance = self.compiler.registry[command](args,
                                                   self.compiler,
                                                   self.scope)
        instance.opens_with_block = True
        self.compiler.context_stack.append(instance)
        return instance.apply()


class Compiler(object):
    def __init__(self, preprocessor, registry=Command.registry):
        self.preprocessor = preprocessor
        self.file_stack = []
        self.context_stack = []
        self.insert_queue = []
        self.registry = registry
        self.root_dir = abspath('./.dpp')
        # manually register some builtins
        registry.update({
            getattr(cls_, 'name', cls_.__name__.upper()): cls_
            for cls_ in (SourceFile, End, With)
        })

    @property
    def source_file(self):
        return self.file_stack[-1][0] if self.file_stack else None

    @property
    def source_dir(self):
        return dirname(self.source_file) if self.source_file else None

    @property
    def context_dir(self):
        basedir = (self.context_root,)
        if self.file_stack:
            rootdir = abspath(dirname(self.file_stack[0][0]))
            curdir = abspath(dirname(self.source_file))
            if len(self.file_stack) > 1 and curdir != rootdir:
                basedir += (basename(curdir),)
        return join(*basedir)

    @property
    def context_root(self):
        return self._get_context(self.file_stack and self.file_stack[0][0])

    def _get_context(self, dockerfile_in_path):
        basedir = (self.root_dir, 'contexts',)
        if dockerfile_in_path:
            basedir += (basename(abspath(dirname(dockerfile_in_path))),)
        return join(*basedir)

    def compile(self, path):
        path = FileReader.resolve_path(path)
        context_dir = self._get_context(path)
        if exists(context_dir):
            rmtree(context_dir)
        makedirs(context_dir)
        with open(join(context_dir, 'Dockerfile'), 'w') as dockerfile:
            for line in self._compile(path):
                dockerfile.write("%s\n" % line)
                yield line

    def _compile(self, path):
        path = FileReader.resolve_path(path)
        return self._yield_from_workaround(self._transform_stream(chain(
            [('', 'SOURCE_FILE %s' % path)],
            self.preprocessor.preprocess(path),
            [('', 'END')]
        )))

    def _transform_stream(self, stream):
        scope = {'workdir': '/', 'shell': ['/bin/sh', '-c']}
        for raw, command in stream:
            command_name, args = self.split_args(command)

            # Add debugging comments to the resulting dockerfile
            if raw:
                yield self._raw_input_comment(raw)

            # Process the input
            if command_name in self.registry:
                instance = self.registry[command_name](args, self, scope)
                yield ("# ----",)
                yield self._filter(instance.apply())
            elif raw:
                yield ("# ----",)
                yield self._filter([raw])

    def split_args(self, command):
        command, args = (re.split(r"\s+", command.strip(), 1) + [''])[:2]
        command_name = PRIVATE.sub(r'\1', command.upper().strip())
        return command_name, args

    def split_files(self, args):
        return re.split(r'\s+', args)

    def insert(self, *content):
        self.insert_queue.extend(content)

    def add_context(self, path, dest_hint=None):
        if isabs(path):
            rel = relpath(path, self.source_dir)
        else:
            rel = path
        source = join(self.source_dir, rel)
        if not abspath(source).startswith(abspath(self.source_dir)):
            rel = basename(rel)
            if rel in ('.', '..'):
                rel = basename(abspath(source)) or 'root'
                if dest_hint:
                    rel = basename(dest_hint)
        context = join(self.context_dir, rel)
        if not isdir(dirname(context)):
            makedirs(dirname(context))
        print(":: %s => %s" % (source, context))
        self.insert("# :: %s => %s" % (source, context))

        try:
            link(source, context)
        except:
            if isdir(source):
                copytree(source, context, True, lambda path, files: [
                    f for f in files
                    if abspath(join(path, f)) == self.root_dir
                ])
            else:
                copy2(source, context)

        return relpath(context, self.context_root)

    def _filter(self, stream):
        for item in ([] if stream is None else stream):
            if isinstance(item, basestring):
                yield item
            else:
                yield " ".join([
                    term if isinstance(term, basestring) else dumps(term)
                    for term in item])

    def _raw_input_comment(self, raw):
        lines = raw.strip().split("\n")
        if self.file_stack:
            path, line = self.file_stack[-1]
            self.file_stack[-1][-1] += len(lines)
            yield "\n# ---- %s:%d" % (path, line)
        for line in lines:
            yield "# -- " + line

    def _yield_from_workaround(self, stream):
        for source in stream:
            for entry in source:
                for extra in self.insert_queue:
                    yield extra
                self.insert_queue = []
                yield entry
