#!/usr/bin/env python
# -*- coding: utf-8 -*-

from itertools import chain, takewhile
import os
import re
import sys

try:
    from subprocess32 import check_output
except ImportError:  # pragma: nocover
    from subprocess import check_output


def macro(pre, post=None):
    def decorator(fn):
        if not hasattr(fn, '_macro_declarations'):
            fn._macro_declarations = set()
        fn._macro_declarations.add((pre, post))
        return fn
    return decorator


class CharStream(object):
    def __init__(self, line_stream):
        self.iterators = [iter(line_stream)]
        self.buffer = []

    def __iter__(self):
        return self

    def __next__(self):
        while not self.buffer:
            try:
                self.buffer.extend(next(self.iterators[0]))
            except StopIteration:
                self.iterators.pop(0)
                if not self.iterators:
                    raise
        return self.buffer.pop(0)

    next = __next__

    def shift(self, stream):
        self.iterators.insert(0, iter(self.buffer))
        self.iterators.insert(0, iter(stream))
        self.buffer = []


class Preprocessor(object):
    def __init__(self, reader):
        self.reader = reader
        self.macros = {
            pre: (post, getattr(self, name))
            for name, fn in type(self).__dict__.items()
            for pre, post in getattr(fn, '_macro_declarations', [])
        }

    def preprocess(self, path):
        raw_stream = self._scan(CharStream(self.reader.read(path)))
        for command in raw_stream:
            from_user = command
            filtered = re.sub(r'\s*\\\n\s*', ' ', command).strip()
            if filtered.startswith('__'):
                from_user = ''
            yield from_user, filtered

    def _scan(self, charstream, ending=None):
        options = [
            (pre, pre, post, None)
            for pre, (post, fn) in self.macros.items()
        ]
        if ending is not None:
            options.append((ending, ending, None, None))
        matches = []
        buf, hash_pos, sloc = [], sys.maxsize, False
        for char in charstream:
            matches = [
                (pre, left[1:], post, len(buf) if start is None else start)
                for pre, left, post, start in chain(matches, options)
                if (not left) or left[0] == char
            ]
            if hash_pos >= len(buf):
                if char == '#':
                    hash_pos = len(buf)
                elif re.match(r'\w', char):
                    sloc = True

            buf.append(char)

            if len(matches) == 1 and not matches[0][1]:
                pre, _, post, start = matches.pop()
                charstream.shift(buf[start+len(pre):])
                buf[start:] = []
                if pre == ending:
                    break
                content = ''
                if post:
                    content = ''.join(self._scan(charstream, post))
                if hash_pos >= start:
                    hash_pos = sys.maxsize
                charstream.shift(self.macros[pre][1](content, sloc))

            if char == "\n":
                buf[min(len(buf) - 1, hash_pos):] = ["\n"]
                tail = set(takewhile((lambda b: b != "\\"), reversed(buf)))
                if tail - set('\t\n ') and sloc:
                    yield ''.join(buf)
                    buf, sloc = [], False
                hash_pos = sys.maxsize
        if buf and sloc:
            yield ''.join(buf[:hash_pos])

    @macro('#{', '}')
    def expand(self, content, sloc):
        content, default = (content.split(":-", 1) + [''])[:2]
        yield os.environ.get(content, default)

    @macro('#include "', '"')
    @macro('#include <', '>')
    @macro('#<', '>')
    def include(self, content, sloc):
        if not sloc:
            yield '\n__SOURCE_FILE__ %s\n' % self.reader.resolve_path(content)
        for n, line in enumerate(self.reader.read(content)):
            if n:
                yield "\\\n" if sloc else "\n"
            yield line.rstrip()
        if not sloc:
            yield '\n__END__\n'

    @macro('#(', ')')
    def output_of(self, content, sloc):
        yield check_output(['sh', '-c', content]).decode('utf-8').strip()
