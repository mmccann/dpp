#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os import makedirs
from os.path import exists, join
from unittest import TestCase

from dpp.commands import Add, Copy

from tests.utils import FakeCompiler, Utils


class AddOrCopyEvaluator(Utils):
    def setUp(self):
        self.fake_compiler = FakeCompiler(self)
        self.scope = {'workdir': '/context/'}
        self.fake_compiler.context_dir = self.make_folder()
        self.fake_compiler.context_root = self.fake_compiler.context_dir
        self.fake_compiler.source_dir = folder = join(self.make_folder(), 'x')
        makedirs(folder)
        open(join(folder, 'y'), 'w').close()
        open(join(folder, '..', 'z'), 'w').close()

    def test_passthrough(self):
        self.assertCommandsProduce(
            '%s y /context/' % self.verb,
            [self.target_class('y', self.fake_compiler, self.scope)]
        )
        self.assertContextHas('y')

    def test_outside_dir(self):
        self.assertCommandsProduce(
            '%s z /context/' % self.verb,
            [self.target_class('../z', self.fake_compiler, self.scope)]
        )
        self.assertContextHas('z')


class AddTests(AddOrCopyEvaluator, TestCase):
    target_class = Add
    verb = 'ADD'

    def test_url_passthrough(self):
        self.assertCommandsProduce("ADD http://google.com /",
                                   [Add('http://google.com /',
                                        self.fake_compiler,
                                        self.scope)])


class CopyTests(AddOrCopyEvaluator, TestCase):
    target_class = Copy
    verb = 'COPY'
