#!/usr/bin/env python
# -*- coding: utf-8 -*-

from unittest import TestCase

from dpp.commands import From
from tests.utils import Utils

class FromTests(TestCase, Utils):
    def test_subsequent_ignored(self):
        scope = {}
        self.assertCommandsProduce('FROM foo', [
            From('foo', None, scope),
            From('bar', None, scope),
            From('baz', None, scope)
        ])

