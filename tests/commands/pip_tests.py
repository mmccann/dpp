#!/usr/bin/env python
# -*- coding: utf-8 -*-

from unittest import TestCase

from dpp.commands import Pip2, Pip3
from tests.utils import Utils

pip_template = r"""
RUN if ! which {2} >/dev/null;
then
    apt-get -qq update --fix-missing &&
    apt-get -qy -o=Dpkg::Use-Pty=0 install {0} &&
    rm -rf /var/lib/apt/lists/* &&
    {1} pip &&
    {2} install --upgrade pip;
fi && {2} install foo bar baz
""".strip().replace("\n", "\\\n")

class PipTests(TestCase, Utils):
    def test_pip2(self):
        self.assertCommandsProduce(pip_template.format('python-setuptools',
                                                       'easy_install',
                                                       'pip2'),
                                   [Pip2('foo bar baz', None, {})])

    def test_pip3(self):
        self.assertCommandsProduce(pip_template.format('python3-setuptools',
                                                       'easy_install3',
                                                       'pip3'),
                                   [Pip3('foo bar baz', None, {})])

