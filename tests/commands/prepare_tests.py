#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os.path import exists, join
from unittest import TestCase

from dpp.commands import Prepare

from tests.utils import Utils, FakeCompiler


class PrepareTests(TestCase, Utils):
    def setUp(self):
        self.fake_compiler = FakeCompiler(self)
        self.fake_compiler.context_dir = self.make_folder()

    def test_runs_command(self):
        cmd = Prepare('touch touchy', self.fake_compiler, {})
        path = join(self.fake_compiler.context_dir, 'touchy')
        self.assertFalse(exists(path))
        self.assertCommandsProduce("", [cmd])
        self.assertTrue(exists(path))

    def test_makes_context_dir_if_needed(self):
        self.fake_compiler.context_dir += '/nonexistant'
        Prepare('touch touchy', self.fake_compiler, {}).apply()
        self.assertTrue(exists(join(self.fake_compiler.context_dir, 'touchy')))

    def test_sets_src_dir_env_var(self):
        self.fake_compiler.source_dir = self.make_folder()
        Prepare('touch $SOURCE/file', self.fake_compiler, {}).apply()
        self.assertTrue(exists(join(self.fake_compiler.source_dir, 'file')))
