#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os import mkdir
from os.path import exists, join
from unittest import TestCase

from dpp.commands import Script
from tests.utils import FakeCompiler, Utils


class ScriptTests(TestCase, Utils):
    def setUp(self):
        self.fake_compiler = FakeCompiler(self)
        self.scope = {'workdir': '/context/'}
        self.fake_compiler.context_dir = self.make_folder()
        self.fake_compiler.context_root = self.fake_compiler.context_dir
        self.fake_compiler.source_dir = folder = self.make_folder()
        for filename in {'foo', 'baz'}:
            open(join(folder, filename), 'w').close()
        self.fake_compiler.missing_files.add('bar')

    def test_single_arg(self):
        self.scope['workdir'] = '/context/subf/'
        ctx = self.fake_compiler.context_dir
        self.fake_compiler.context_dir += "/subf/"
        self.assertCommandsProduce("""
            COPY subf/foo /context/subf/
            RUN cd /context/subf/ \
             && chmod +x ./foo && sync && ./foo
        """, [Script('foo', self.fake_compiler, self.scope)])
        self.fake_compiler.context_dir = ctx
        self.assertContextHas('subf/foo')

    def test_multi_arg(self):
        self.assertCommandsProduce("""
            COPY foo baz /context/
            RUN cd /context/ \
             && chmod +x ./foo && sync && ./foo bar baz
        """, [Script('foo bar baz', self.fake_compiler, self.scope)])
        self.assertContextHas('foo')
        self.assertContextHas('baz')

    def test_missing_script(self):
        with self.assertRaises(OSError):
            Script("bar", self.fake_compiler, self.scope).apply()
