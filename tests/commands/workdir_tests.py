#!/usr/bin/env python
# -*- coding: utf-8 -*-

from unittest import TestCase

from dpp.commands import Workdir
from tests.utils import Utils


class WorkdirTests(TestCase, Utils):
    def setUp(self):
        self.scope = {'workdir': '/doe'}

    def test_apply(self):
        self.assertCommandsProduce("""
            WORKDIR /foo
        """, [Workdir('/foo', None, self.scope)])
        self.assertEqual("/foo", self.scope['workdir'])

    def test_reverse(self):
        instance = Workdir('/foo', self, self.scope)
        list(instance.apply() or [])
        self.assertEqual("/foo", self.scope['workdir'])
        list(instance.remove() or [])
        self.assertEqual("/doe", self.scope['workdir'])
