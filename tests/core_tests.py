#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os.path import join
from unittest import TestCase

from dpp.core import FileReader
from tests.utils import Utils


class FileReaderTests(TestCase, Utils):
    def test_resolve_path__exists(self):
        path = join(self.make_folder(), 'real_file')
        open(path, 'w').close()
        self.assertEqual(FileReader.resolve_path(path), path)

    def test_resolve_path__is_dir_with_dockerfile_in(self):
        folder = self.make_folder()
        path = join(folder, 'Dockerfile.in')
        open(join(folder, 'Dockerfile'), 'w').close()
        open(path, 'w').close()
        self.assertEqual(FileReader.resolve_path(folder), path)

    def test_resolve_path__is_dir_with_dockerfile(self):
        folder = self.make_folder()
        path = join(folder, 'Dockerfile')
        open(path, 'w').close()
        self.assertEqual(FileReader.resolve_path(folder), path)
