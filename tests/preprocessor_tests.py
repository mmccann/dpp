#!/usr/bin/env python
# -*- coding: utf-8 -*-

from unittest import TestCase
import re
from os import environ
from textwrap import dedent
from dpp.preprocessor import CharStream, Preprocessor


class FakeFileReader(object):
    def __init__(self, **kw):
        self.content = kw
    def read(self, path):
        return self.content[path].splitlines(True)
    @staticmethod
    def resolve_path(path):
        return path

class CharStreamTests(TestCase):
    def setUp(self):
        self.target = CharStream(['do', 're', 'mi', 'fa', 'so'])

    def test_iterator_nonrepeatable(self):
        for a in self.target:
            self.assertEqual('d', a)
            break
        for a in self.target:
            self.assertEqual('o', a)
            break

    def test_shift(self):
        out = []
        for a in self.target:
            out.append(a)
            if a in 'aoeui':
                self.target.shift('! ')
        self.assertEqual('do! re! mi! fa! so! ', ''.join(out))

    def test_shift_stream(self):
        def fake_stream():
            yield 'FEE'
            yield 'FIE'
            yield 'F'
        for a in self.target:
            self.target.shift(fake_stream())
            break
        self.assertEqual('FEEFIEForemifaso', ''.join(self.target))


class PreprocessorTests(TestCase):
    def setUp(self):
        self.file_reader = FakeFileReader(kitty="meow\nmew\npurr\n")
        self.target = Preprocessor(self.file_reader)

    def assertStringSimilar(self, expected, actual):
        self.assertEqual(*[
            '\n'.join([
                line.strip()
                for line in re.sub(r'\n\s*\n', '\n', sample).splitlines()
            ]).strip()
            for sample in [expected, actual]
        ])

    def assertPreprocessesTo(self, expected, source, command_count):
        self.file_reader.content['input'] = source
        raw, filtered = zip(*self.target.preprocess('input'))
        actual = ''.join(raw)
        self.assertEqual(command_count, len(filtered))
        self.assertStringSimilar(re.sub(r'(?m)^\s*__\w+__.*$', '', expected),
                                 actual)
        self.assertListEqual(
            [
                re.sub(r"[ \t]+", ' ', line.strip())
                for line in re.sub(r'\\\n\s*', ' ', expected).split("\n")
                if line.strip()
            ],
            list(filtered)
        )

    def test_expand(self):
        environ['NOTE'] = 'doe'
        self.assertPreprocessesTo(r"""
            doe ray me fa so la tea doe
        """, r"""
            #{NOTE} ray me fa so la tea #{NOTE}
        """, 1)

    def test_expand_nested(self):
        environ.update({'FOO': 'BAR', 'FOOBAR': 'CHEESE'})
        self.assertPreprocessesTo(r'CHEESE', '#{FOO#{FOO}}', 1)

    def test_expand_default(self):
        environ.pop('UNSET', '')
        self.assertPreprocessesTo(r'Oh No!', '#{UNSET:-Oh No!}', 1)

    def test_output_of(self):
        self.assertPreprocessesTo(
            r'yes yes',
            r'#(echo indeed | sed "s,...,yes ,g")',
            1
        )

    def test_include(self):
        self.assertPreprocessesTo('The cat says meow\\\nmew\\\npurr',
                                  r'The cat says #include <kitty>', 1)
        self.assertPreprocessesTo('The cat says meow\\\nmew\\\npurr',
                                  r'The cat says #include "kitty"', 1)
        self.assertPreprocessesTo('The cat says meow\\\nmew\\\npurr',
                                  r'The cat says #<kitty>', 1)

    def test_block_include(self):
        self.assertPreprocessesTo(
            'The cat says:\n__SOURCE_FILE__ kitty\nmeow\nmew\npurr\n__END__',
            'The cat says:\n#include <kitty>', 6)

    def test_swallows_comments(self):
        environ['FOO'] = 'BAR'
        self.assertPreprocessesTo(r'''
            BAR
            run BAR\

            BAR
        ''', r'''
            # leading comment
            #{FOO} # environment variable FOO as #{FOO}
            run #{FOO}\
              # comment in a line continuation
            #{FOO}
        ''', 2)

    def test_line_continuation(self):
        self.assertPreprocessesTo(r'''
            APT meow\
                mew\
                purr\

                && rm -rf /var/www
        ''', r'''
            APT #<kitty>\
                # Remove any dangling default Apache files
                && rm -rf /var/www
        ''', 1)

