#!/usr/bin/env python
# -*- coding: utf-8 -*-

from json import dumps
from os import walk
from os.path import abspath, basename, dirname, exists, join, relpath
import re
from shutil import rmtree
from tempfile import mkdtemp
from unittest import TestCase

from dpp.core import *

try:
    FileNotFoundError
except NameError:
    FileNotFoundError = OSError


class FakeCompiler(object):
    def __init__(self, test):
        self.test = test
        self.missing_files = set()
        self.compile_output = []

    def split_args(self, command):
        command = command.strip()
        return (command or " ").split(" ", 1)

    def split_files(self, files):
        return re.split(r"\s+", files)

    def insert(self, *content):
        pass

    def add_context(self, path, dest_hint=None):
        if basename(path) in self.missing_files:
            raise FileNotFoundError(path)
        if not self.test._context_files:
            self.test._context_files = set()
        self.test._context_files.add(basename(path))
        rel = relpath(self.context_dir, self.context_root)
        return join(rel, basename(path)) if rel != '.' else basename(path)

    def _compile(self, path):
        return self.compile_output


class Utils(object):
    _context_files = None

    def _sloc(self, text):
        text = re.sub(r'#.*', '', text)
        return [y for y in [
            re.sub(r'\s+', ' ', x.strip())
            for x in re.sub(r'\s*\\\s*\n\s*', ' ', text.strip()).split("\n")
        ] if y]

    def assertContextHas(self, name):
        self.assertIn(basename(name), self._context_files or {})

    def assertContextDoesntHave(self, name):
        self.assertNotIn(basename(name), self._context_files or {})

    def assertDockerfilesEquivalent(self, expected, actual):
        self.assertListEqual(self._sloc(expected), self._sloc(actual))

    def assertCommandsProduce(self, expected, commands):
        self.assertDockerfilesEquivalent(expected, "\n".join(
            " ".join(term if isinstance(term, str) else dumps(term)
                     for term in instruction)
            for command in commands
            for instruction in command.apply() or []
        ))

    def make_folder(self):
        out = mkdtemp()
        if not getattr(self, '_tempdir', None):
            self._tempdir = set()
        self._tempdir.add(out)
        return out

    def tearDown(self):
        tempdir = getattr(self, '_tempdir', set())
        for folder in tempdir:
            rmtree(folder)
        self._context_files = None


